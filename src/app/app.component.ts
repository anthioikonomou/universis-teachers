import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ConfigurationService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ApplicationSettings } from './teachers-shared/teachers-shared.module';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private _router: Router,
    public context: AngularDataContext,
    private _config: ConfigurationService,
    public translate: TranslateService,
    private _titleService: Title) {

    if (this._config.settings?.i18n && this._config.settings?.i18n.defaultLocale) {
      translate.setDefaultLang(this._config.settings?.i18n.defaultLocale);
    } else {
      translate.setDefaultLang('en');
    }
    translate.use(this._config.currentLocale);
  }

  public setTitle(newTitle: string) {
    this._titleService.setTitle(newTitle);
  }

  ngOnInit() {
    const customTitle = (<ApplicationSettings>this._config.settings.app) && (<ApplicationSettings>this._config.settings.app).title;

    if (customTitle && customTitle.trim() !== '') {
      this.setTitle(customTitle);
    }
  }

}
