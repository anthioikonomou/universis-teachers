import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-course-class-absence-limit',
  template: `
    <form>
      <div class="col-12 mb-3" [translate]="'AbsenceLimit.Description'"></div>
      <div class="form-group col-12 col-md-7">
        <input type="text"
          class="form-control col text-left"
          [(ngModel)]="absenceLimit"
          [formControl]="absenceForm"
          pattern="^[0-9]+$">
        <div *ngIf="absenceForm.errors?.['pattern']" class="text-danger mt-3" [translate]="'AbsenceLimit.InvalidValue'"></div>
      </div>
      <div class="text-secondary col-12" [translate]="'AbsenceLimit.Notes'"></div>
    </form>`
})
export class CourseClassAbsenceLimitComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  @Input() absenceLimit?: number | string;
  @Input() courseClassId?: number | string;
  public absenceForm = new FormControl();
  private dataSubscription: Subscription | any;
  private formStatusSubscription: Subscription | any;


  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private toastService: ToastService) {

    super(router, route);
    this.modalTitle = this.translate.instant('AbsenceLimit.Title');
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe(data => {
      this.absenceLimit = data['courseClass'].absenceLimit;
      this.courseClassId = data['courseClass'].id;
    });
    this.formStatusSubscription = this.absenceForm.statusChanges.subscribe(status => {
      this.okButtonDisabled = !(status === 'VALID');
    });
  }

  ok(): Promise<any> {
    return this._context.model(`instructors/me/classes/${this.courseClassId}/absenceLimit`)
      .save({ absenceLimit: Number(this.absenceLimit || 0) })
      .then(() => {
        // show success toast message
        this.toastService.show(
          this.translate.instant('AbsenceLimit.SuccessTitle'),
          this.translate.instant(('AbsenceLimit.SuccessMessage'))
        );
        // close modal
        this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }).catch(err => {
        // show fail toast message
        this.toastService.show(
          this.translate.instant('AbsenceLimit.FailTitle'),
          this.translate.instant(('AbsenceLimit.FailMessage'))
        );
        // close modal without triggering a reload
        this.cancel();
      });
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.formStatusSubscription) {
      this.formStatusSubscription.unsubscribe();
    }
  }

}
