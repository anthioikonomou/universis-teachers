import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-courses-details-external-links',
  template:`
    <form>
      <div class="col-12 mb-3" [translate]="'ExternalLinksModal.Description'"></div>
      <div class="form-group col-12 col-md-7">
        <label for="studyGuideUrl" class="text-gray-600" [translate]="'CoursesLocal.ClassURL'"></label>
        <input type="url" id="studyGuideUrl"
          class="form-control col text-left mb-2"
          [(ngModel)]="studyGuideUrl"
          [formControl]="studyGuideUrlFormControl" pattern='^(https|ftp):\/\/[^ "]+$'>
        <div *ngIf="studyGuideUrlFormControl.errors?.['pattern']" class="text-danger mt-3" [translate]="'ExternalLinksModal.InvalidValue'"></div>
        <label for="eLearningUrl" class="text-gray-600" [translate]="'CoursesLocal.Elearning'"></label>
        <input type="url" id="eLearningUrl"
          class="form-control col text-left"
          [(ngModel)]="eLearningUrl"
          [formControl]="eLearningUrlFormControl" pattern='^(https|ftp):\/\/[^ "]+$'>
        <div *ngIf="eLearningUrlFormControl.errors?.['pattern']" class="text-danger mt-3" [translate]="'ExternalLinksModal.InvalidValue'"></div>
      </div>
    </form>`
})
export class CoursesDetailsExternalLinksComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  @Input() studyGuideUrl?: URL | string;
  @Input() eLearningUrl?: URL | string;
  @Input() courseClassId?: number | string;

  public studyGuideUrlFormControl = new FormControl();
  public eLearningUrlFormControl = new FormControl();
  private dataSubscription: Subscription;
  private studyGuideUrlStatusSubscription: Subscription;
  private eLearningUrlStatusSubscription: Subscription;


  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private toastService: ToastService) {

    super(router, route);
    this.modalTitle = this.translate.instant('ExternalLinksModal.Title');
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe(data => {
      this.courseClassId = data['courseClass'].id;
      this.studyGuideUrl = data['courseClass'].statistic?.studyGuideUrl;
      this.eLearningUrl = data['courseClass'].statistic?.eLearningUrl;
    });
    this.studyGuideUrlStatusSubscription = this.studyGuideUrlFormControl.statusChanges.subscribe(status => {
      this.okButtonDisabled = status !== 'VALID';
    });
    this.eLearningUrlStatusSubscription = this.eLearningUrlFormControl.statusChanges.subscribe(status => {
      this.okButtonDisabled = status !== 'VALID';
    });
  }

  async ok(): Promise<any> {
    try {
      await this._context.model(`instructors/me/classes/${this.courseClassId}/links`)
        .save({
          studyGuideUrl: this.studyGuideUrl,
          eLearningUrl: this.eLearningUrl
        });
      // show success toast message
      this.toastService.show(
        this.translate.instant('ExternalLinksModal.SuccessTitle'),
        this.translate.instant(('ExternalLinksModal.SuccessMessage'))
      );
      // close modal
      this.close({
        fragment: 'reload',
        skipLocationChange: true
      });
    } catch (err) {
      // show fail toast message
      if (err.status === 403) {
        this.toastService.show(
          this.translate.instant('ExternalLinksModal.FailTitle'),
          this.translate.instant(('ExternalLinksModal.InsufficientPermission'))
        );
      } else {
        this.toastService.show(
          this.translate.instant('ExternalLinksModal.FailTitle'),
          this.translate.instant(('ExternalLinksModal.FailMessage'))
        );
      }
      // close modal without triggering a reload
      this.cancel();
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.eLearningUrlStatusSubscription) {
      this.eLearningUrlStatusSubscription.unsubscribe();
    }
    if (this.studyGuideUrlStatusSubscription) {
      this.studyGuideUrlStatusSubscription.unsubscribe();
    }
  }
}
