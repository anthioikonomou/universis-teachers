import { Component, OnInit, ElementRef, ViewChild, TemplateRef, Output, EventEmitter, AfterViewInit, Input } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService, ModalService, ToastService, LoadingService } from '@universis/common';
import { ClientDataQueryable, ResponseError } from '@themost/client';
import { MessagesService } from '../../../messages/services/messages.service';
import { ProfileService } from '../../../profile/services/profile.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SendMessageToClassComponent } from '../../../messages/components/send-message-to-class/send-message-to-class.component';
import { Observable, Subscription } from 'rxjs';
import { ActivatedTableService, AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedTableConfiguration } from '@universis/ngx-tables';
import * as STUDENTS_LIST_CONFIG from './course-details-students.config.list.json';
import * as SEARCH_LIST_CONFIG from './course-details-students-table.search.list.json';
import { HttpClient } from '@angular/common/http';
import { SendMessageActionComponent } from '../../../consulted-students/components/send-message-action/send-message-action.component';


@Component({
  selector: 'app-courses-details-students',
  templateUrl: './courses-details-students.component.html',
  styleUrls: ['./courses-details.component.scss'],
})
export class CoursesDetailsStudentsComponent implements AfterViewInit {
  bsModalRef: BsModalRef | any;
  public selectedClass: any;
  public selectedClassSection = null;
  selector = '.inner__content';
  @ViewChild('table') table!: AdvancedTableComponent;
  @ViewChild('search') search!: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch!: AdvancedTableSearchComponent;
  @Input() searchConfiguration: any;
  public recordsTotal: any;
  public showMessageForm = false;

  public searchText = '';
  public loggedInstructor: any;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('innerContainer') innerContainer!: ElementRef;
  @ViewChild('templateMsg') msgTemplate!: TemplateRef<any>;
  @ViewChild('templateClassMsg') classMsgTemplate!: TemplateRef<any>;
  @Output() succesfulSend = new EventEmitter<boolean>();
  @ViewChild('fileinput') fileinput;
  public messageModel = {
    body: "",
    file: null,
    subject: ""
  };
  private dataSubscription?: Subscription;
  public selectedItems: any[] = [];

  constructor(
    private _loadingService: LoadingService,
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _coursesService: CoursesService,
    private _errorService: ErrorService,
    private _route: ActivatedRoute,
    private _profileService: ProfileService,
    private _bsModalService: BsModalService,
    private _toastService: ToastService,
    private _http: HttpClient,
    private _translateService: TranslateService) {
  }

  ngAfterViewInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe((data: any) => {
      this._route.parent?.params.subscribe(routeParams => {
        this._coursesService.getCourseClass(routeParams['course'], routeParams['year'], routeParams['period']).then(courseClass => {
          if (typeof courseClass === 'undefined') {
            return this._errorService.navigateToError(new ResponseError('Course class cannot be found or is inaccessible', 404));
          }
          this.searchConfiguration = Object.assign({}, SEARCH_LIST_CONFIG, { customOptions: { courseClass: courseClass.id } })

          // reset search text
          if (data['tableConfiguration']) {
            // set config
            this.table.config = AdvancedTableConfiguration.cast(data['tableConfiguration']);
            // reset search text
            this.advancedSearch.text = "";
          }
          //this.advancedSearch.text = "";
          // set selected course class
          this.selectedClass = courseClass;
          // get course class students
          this.table.query = this._coursesService.getCourseClassAllStudents(courseClass.id);
          this.table.config = AdvancedTableConfiguration.cast(STUDENTS_LIST_CONFIG);
          this.table.fetch(true);
          this._activatedTable.activeTable = this.table;

        }).catch(err => {
          // hide loading
          this._loadingService.hideLoading();
          return this._errorService.navigateToError(err);
        });
      });
      this._profileService.getInstructor().then((logged) => {
        this.loggedInstructor = logged;
      });
    });

  }

  onDataLoad(event: AdvancedTableDataResult) {
    this.recordsTotal = event.recordsTotal;
  }

  executeSendDirectMessageAction(message: any) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems?.length;
      // collect all students id which selected by instructor
      const selectedStudentsId = this.selectedItems.map(el => el.student.id)
    
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      let errors = 0;
      // execute promises in series within an async method
      (async () => {
          try {
            const sendMessage: FormData = new FormData();
            // condition for attachment file at message form and send it to sendMessage
            if (message.attachments && Array.isArray(message.attachments) && message.attachments.length === 1) {
              sendMessage.append('file', message.attachments[0], message.attachments[0].name || 'attachment');
            }
            // send the body from message form to sendMessage
            sendMessage.append('body', typeof (message.body) === 'undefined' ? '' : message.body);
            // send the subject from message form to sendMessage
            sendMessage.append('subject', typeof (message.subject) === 'undefined' ? '' : message.subject);

            // tranform selected students id to a json array
            var json_arr = JSON.stringify(selectedStudentsId);
            // send the json array to sendMessage
            sendMessage.append('students', json_arr);
            // get service headers
            const serviceHeaders = this._context.getService().getHeaders();
            // set postUrl
            const postUrl = this._context.getService().resolve(`instructors/me/classes/${this.selectedClass.id}/students/sendMessageToSelectedStudents`);
            // import sheet
            await this._http.post(postUrl, sendMessage, {
              headers: serviceHeaders
            }).toPromise();
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            errors += 1;
          }
       // }
      })().then(() => {
        if (errors === 0) {
          this._toastService.show(this._translateService.instant('Modals.SuccessfulSendMessageTitle'),
          this._translateService.instant('Messages.MessageNotifications.RegisteredEmail'));
        } else {
          const description = errors === 1 ? this._translateService.instant('Modals.FailedSendMessageTitle')
            : this._translateService.instant('Modals.FailedSendMessageMessage');
          this._toastService.show(this._translateService.instant('Modals.FailedSendMessageTitle'),
            `${errors.toString()}  ${description}`);
        }
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async sendDirectMessageAction() {
    if (this.loggedInstructor.email == null) {
      this.openModalWithComponent();
    } else {
    try {
      this._loadingService.showLoading();
      const items = this.selectedItems = await this.getSelectedItems();
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: items,
          // we dont want ActionMessage because we use all selectedItems
          actionMessage: "Messages.ActionEmailMessage",
          modalTitle: 'Messages.SendEmailToStudent.Title',
          description: 'Messages.SendEmailToStudent.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendDirectMessageAction(message)
        }
      });
    } catch (err) {
      console.error(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
    }
  }

  public openModalWithComponent() {
    const initialState = {
      'showMessageForm': !this.showMessageForm,
      'courseClass': this.selectedClass,
      'instructor': this.loggedInstructor
    };
    this.bsModalRef = this._bsModalService.show(SendMessageToClassComponent, { initialState });
  }

  async getSelectedItems() {
    let items: any[] = [];
    if (this.table && this.table.query) {
      const lastQuery: ClientDataQueryable = this.table.query;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'student/id as student'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              student: item.student
            };
          });
        }
      }
    }
    return items;
  }

  exportStudentList(courseClass) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    let fileURL;
    if (this.selectedClassSection || this.searchText) {
      if (this.selectedClassSection && this.searchText) {
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${encodeURIComponent(courseClass.id)}
        /students/export?$filter=section eq ${this.selectedClassSection}
        and (indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      } else if (this.selectedClassSection && !this.searchText) {
        // append class section query
        fileURL = this._context.getService()
          // tslint:disable-next-line:max-line-length
          .resolve(`instructors/me/classes/${encodeURIComponent(courseClass.id)}/students/export?$filter=section eq ${this.selectedClassSection}`);
      } else if (this.searchText && !this.selectedClassSection) {
        // append class section query
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${encodeURIComponent(courseClass.id)}
        /students/export?$filter= indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      }
    } else {
      fileURL = this._context.getService().resolve(`instructors/me/classes/${encodeURIComponent(courseClass.id)}/students/export`);
    }
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        const filtered = this._translateService.instant('CoursesLocal.filtered');
        a.href = objectUrl;
        let downloadName;
        if (this.selectedClassSection || this.searchText) {
          if (this.selectedClassSection && this.searchText) {
            downloadName = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}-${filtered}.xlsx`;
          } else if (this.selectedClassSection && !this.searchText) {
            downloadName = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}.xlsx`;
          } else if (this.searchText && !this.selectedClassSection) {
            downloadName = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${filtered}.xlsx`;
          }
        } else {
          downloadName = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}.xlsx`;
        }
        a.download = downloadName;
        if (window.navigator && (window.navigator as any).msSaveOrOpenBlob) {
          (window.navigator as any).msSaveOrOpenBlob(blob, downloadName);
        } else {
          a.click();
        }
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });
  }

}
