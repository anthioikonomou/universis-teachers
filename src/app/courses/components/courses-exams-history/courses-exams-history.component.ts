import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Router} from '@angular/router';
import {CoursesService} from '../../services/courses.service';
import {ConfigurationService, LoadingService} from '@universis/common';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-courses-exams-history',
  templateUrl: './courses-exams-history.component.html',
  styleUrls: ['./courses-exams-history.component.scss']
})
export class CoursesExamsHistoryComponent implements  AfterViewInit {

  public exams: any = [];
  public select: any = [];
  public isLoading = true;
  public searchText = '';
  public take = 10;
  public skip = 0;
  public count: any;
  public selectedExamStatus = null;
  public selectedExamYear = null;
  public selectedExamPeriod = null;
  private orphanExams = 0; // count of exams without classes
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";

  @ViewChild('innerContainer') innerContainer!: ElementRef;

  constructor(private _context: AngularDataContext,
              private coursesService: CoursesService,
              private errorService: ErrorService,
              private router: Router,
              private loadingService: LoadingService,
              private _configurationService: ConfigurationService) {
                this.currentLanguage = this._configurationService.currentLocale;
                this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;           
  }

  ngAfterViewInit() {
    this.getData();
    this.coursesService.getCourseExamSelectValues()
      .then(res => { this.select = res; })
      .catch(err => this.errorService.navigateToError(err));
  }

  onSearchTextKeyDown($event: any) {
    if ($event.keyCode === 13) {
      this.initializeData().getData();
    }
  }

  onSearchTextKeyUp($event: any) {
    if ($event.target && $event.target.value.length === 0) {
      this.initializeData().getData();
    }
  }

  onScroll() {
    // don't execute query if all exams are loaded
    if (this.exams.length < this.count) {
      this.skip += this.take;
      this.getData();
    }
  }

  selectChangeHandler() {
    this.initializeData().getData();
  }

  initializeData() {
    this.skip = 0;
    this.orphanExams = 0;
    this.exams = [];
    $(this.innerContainer?.nativeElement).scrollTop(0);
    return this;
  }

  loadData(examData) {
    examData = examData || {
      value: [],
      total: 0
    };
    this.exams = [
      ...this.exams,
      ...examData.value.filter(exam => Array.isArray(exam.classes) && exam.classes.length ? true : !(++this.orphanExams))
    ];
    this.count = examData.total - this.orphanExams;
  }

  getData() {
    const query = this.coursesService.searchCourseExams(this.searchText, this.skip, this.take);

    // apply filters
    if (this.selectedExamStatus) { query.and('status/alternateName').equal(this.selectedExamStatus); }
    if (this.selectedExamYear) { query.and('year/alternateName').equal(this.selectedExamYear); }
    if (this.selectedExamPeriod) { query.and('examPeriod/alternateName').equal(this.selectedExamPeriod); }

    // if exams list is empty show loading card
    if (this.exams.length === 0) { this.isLoading = true; }

    // execute query and then add the result to the exams list
    this.loadingService.showLoading();
    return query.getList().then(res => {
      this.isLoading = false;
      this.loadingService.hideLoading();
      this.loadData(res);
    }).catch(err => {
      this.isLoading = false;
      this.errorService.navigateToError(err);
    });
  }

}
