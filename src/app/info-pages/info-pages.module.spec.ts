import { InfoPagesModule } from './info-pages.module';
import { SharedModule } from '@universis/common';

describe('InfoPagesModule', () => {
  let infoPagesModule: InfoPagesModule;

  beforeEach(() => {

    infoPagesModule = new InfoPagesModule();
  });

  it('should create an instance', () => {
    expect(infoPagesModule).toBeTruthy();
  });
});
