import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import { TranslateModule} from '@ngx-translate/core';
import { MessagesService} from '../../services/messages.service';
import {
  ConfigurationService,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService
} from '@universis/common';
import { SendMessageToClassComponent } from './send-message-to-class.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BsModalRef, BsModalService, ModalModule} from 'ngx-bootstrap/modal';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingConfigurationService} from '@universis/common/testing';
import {MostModule} from '@themost/angular';
import {MessageSharedService} from '../../../teachers-shared/services/messages.service';
import { SharedModule } from '@universis/common';

describe('SendMessageToClassComponent', () => {
  let component: SendMessageToClassComponent;
  let fixture: ComponentFixture<SendMessageToClassComponent>;
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        SharedModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
        ModalModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false,
            useResponseConversion: true
          }
        })
      ],
      declarations: [ SendMessageToClassComponent ],
      providers: [
        BsModalService,
        BsModalRef,
        ToastService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        MessagesService,
        MessageSharedService,
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        ModalService,
        ErrorService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToClassComponent);
    component = fixture.componentInstance;
    component.courseClass = Math.round(Math.random() * 1000);
    component.messageModel = {
      body: null,
      file: null,
      subject: null
    };
    fixture.detectChanges();
  });

  it('should create', inject([MessagesService], (_msgSvc: MessagesService) => {
    spyOn(_msgSvc, 'sendMessageToClass').and.callFake(():any => {
      return Promise.resolve(JSON.parse('{"value": []}'));
    });
    expect(component).toBeTruthy();
  }));
});
