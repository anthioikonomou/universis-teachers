import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@universis/common';
import { StudentsRoutingModule } from './students-routing.module';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

// LOCALES: import extra locales here
import * as el from './i18n/students.el.json';
import * as en from './i18n/students.en.json';

@NgModule({
  imports: [
    CommonModule,
    StudentsRoutingModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [StudentsHomeComponent]
})
export class StudentsModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
