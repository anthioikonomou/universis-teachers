import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ThesesService } from '../../services/theses.service';
import { LoadingService } from '@universis/common';
import { ErrorService } from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-theses-currents',
  templateUrl: './theses-currents.component.html',
  styleUrls: ['../theses-completed/theses-completed.component.scss']
})
export class ThesesCurrentsComponent implements OnInit {
  public currentTheses: any = [];
  public isLoading = true;   // Only if data is loaded
  public count: any;
  public searchText = '';
  public selectedThesisType = null;
  public select: any = [];
  public instructor: any;

  constructor(private thesesServices: ThesesService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    this._loadingService.showLoading();

    this.profileService.getInstructor().then(res => {
      this.instructor = res;
    }).catch( err => {
      return this._errorService.navigateToError(err);
    });
    
    this.thesesServices.getCurrentTheses().then((res: any) => {
      this.currentTheses = this.updateResultsInCurrentTheses(res);
      this.count = res.length;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }).catch(err => {
      // hide loading
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
    this.thesesServices.getCurrentThesesSelectValues()
      .then((res: any) => { this.select = res; })
      .catch((err: any) => this._errorService.navigateToError(err));
  }

  search() {
    const query = this.thesesServices.searchCurrentTheses(this.searchText);
    if (this.selectedThesisType) {
      query.and('type/alternateName').equal(this.selectedThesisType);
    }
    try {
      this.isLoading = true;
      this._loadingService.showLoading();
      query.getItems().then((res: any) => {
        this.currentTheses = this.updateResultsInCurrentTheses(res);
        this.count = res.length;
        this.isLoading = false;
        this._loadingService.hideLoading();
      }).catch(err => {
        // hide loading
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });
    } catch (err) {
      this._loadingService.hideLoading();
      console.error(err);
    }
  }

    // Update results within current theses - instructor role
    updateResultsInCurrentTheses(theses) {
      theses.forEach(thesis => {
         thesis.students.forEach(student => {
           student.results.forEach(result => {
             const instructor = thesis.members.find(item => item.member.id === result.instructor.id);
               result.instructor.roleName = instructor && instructor.roleName ? instructor.roleName : null;
           });
           student.results.sort((a, b) => {
            return a.index < b.index ? -1 : 1;
          });
         });
       });
       return theses;
     }

}
