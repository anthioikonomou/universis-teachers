import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesisProposalOverviewComponent } from './thesis-proposal-overview.component';

describe('ThesisProposalOverviewComponent', () => {
  let component: ThesisProposalOverviewComponent;
  let fixture: ComponentFixture<ThesisProposalOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThesisProposalOverviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThesisProposalOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
