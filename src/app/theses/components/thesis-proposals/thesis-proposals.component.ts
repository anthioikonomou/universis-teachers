import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { LoadingService, ErrorService, ToastService } from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { ThesesService } from '../../services/theses.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-thesis-proposals',
  templateUrl: './thesis-proposals.component.html',
  styleUrls: ['../theses-completed/theses-completed.component.scss']

})
export class ThesisProposalsComponent implements OnInit, OnDestroy{

  private paramSubscription: Subscription;
  public thesisProposals: any = [];
  public isLoading = true;
  public count: any;
  public searchText = '';
  public selectedThesisType = null;
  public select: any = [];
  public instructor: any;
  public item: any;
  public modalRef!: BsModalRef;

  

  constructor(private thesesServices: ThesesService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private profileService: ProfileService, 
    private _activatedRoute: ActivatedRoute,
    private _modalService: BsModalService,
    private _context: AngularDataContext,
    private _toastService: ToastService,
    private _translate: TranslateService
  ) { }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.profileService.getInstructor().then(res => {
      this.instructor = res;
    }).catch( err => {
      return this._errorService.navigateToError(err);
    });
    
    this.loadData();

    this.paramSubscription = this._activatedRoute.fragment.subscribe((data)=> { 
      if (data === 'reload') {
        this.loadData();
      }
    });

    this.thesesServices.getThesisProposalsSelectValues()
      .then((res: any) => { this.select = res; })
      .catch((err: any) => this._errorService.navigateToError(err));
  }

  loadData() {
    this.thesesServices.getThesisProposals().then(async (res: any) => {
      this.thesisProposals = res.map(el => {
        Array.isArray(el.students) ? Object.assign(el, { interest: el.students.length }) : Object.assign(el, { interest: null });
        return el;
      });
      this.count = res.length;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }).catch(err => {
      // hide loading
      console.log(err);
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  search() {
    const query = this.thesesServices.searchThesisProposals(this.searchText);
    if (this.selectedThesisType) {
      query.and('type/alternateName').equal(this.selectedThesisType);
    }
    try {
      this.isLoading = true;
      this._loadingService.showLoading();
      query.getItems().then((res: any) => {
        this.thesisProposals = res.map(el => {
          Array.isArray(el.students) ? Object.assign(el, { interest: el.students.length }) : Object.assign(el, { interest: null });
          return el;
        });
        this.count = res.length;
        this.isLoading = false;
        this._loadingService.hideLoading();
      }).catch(err => {
        // hide loading
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });
    } catch (err) {
      this._loadingService.hideLoading();
      console.error(err);
    }
  }


  onRemove(templateToLoad: TemplateRef<any>, thesis: any) {
    this.item = thesis;
    this.modalRef = this._modalService.show(templateToLoad);
  }

  remove(data: any) {
    this.modalRef.hide();
    return this._context.model(`instructors/me/removeThesisProposal`).save(this.item)
      .then(async (res) => { 
        return this.loadData();
      }).catch((err: any) => {
        console.log(err);
        this._loadingService.hideLoading();
        // show fail toast message
        this._toastService.show(
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailToRemove'),
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailToRemoveMessage')
        );
      });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    } 
  }

}
