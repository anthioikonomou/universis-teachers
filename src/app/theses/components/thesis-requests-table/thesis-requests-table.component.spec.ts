import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesisRequestsAdvancedTableComponent } from './thesis-requests-table.component';

describe('ThesisRequestsAdvancedTableComponent', () => {
  let component: ThesisRequestsAdvancedTableComponent;
  let fixture: ComponentFixture<ThesisRequestsAdvancedTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThesisRequestsAdvancedTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThesisRequestsAdvancedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
