import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {ThesesCompletedComponent} from './components/theses-completed/theses-completed.component';
import {ThesesCurrentsComponent} from './components/theses-currents/theses-currents.component';
import {ThesesAdvancedHomeComponent} from './components/theses-advanced-home/theses-advanced-home.component';
import {ThesesAdvancedTableComponent} from './components/theses-advanced-table/theses-advanced-table.component';
import { ThesesAdvancedTableConfigurationResolver, ThesesAdvancedTableSearchResolver } from './components/theses-advanced-table/theses-advanced-table-config.resolver';
import { AdvancedFormModalData, AdvancedFormResolver } from '@universis/forms';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';
import { EditThesisProposalComponent } from './components/thesis-proposals/edit-thesis/edit-thesis-proposal.component';
import { ThesisProposalOverviewComponent } from './components/thesis-proposals/thesis-proposal-overview/thesis-proposal-overview.component';
import { ThesisProposalsComponent } from './components/thesis-proposals/thesis-proposals.component';


const routes: Routes = [
  {
    path: '',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  },  

  {
    path: 'current',
    component: ThesesCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: [ ]
  },
  {
    path: 'completed',
    component: ThesesCompletedComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'proposals',
    component: ThesisProposalsComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: EditThesisProposalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData> {
          model: 'Instructors/Me/Thesis',
          action: 'editPotential',
          closeOnSubmit: true
        },
        resolve: {
          formConfig: AdvancedFormResolver
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: EditThesisProposalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData> {
          model: 'Instructors/Me/Thesis',
          action: 'edit',
          closeOnSubmit: true
        }
      }
      
    ]
  },
  {
    path: 'proposals/:id/overview',
    pathMatch: 'full',
    component: ThesisProposalOverviewComponent,
    data: {
      model: 'ThesisProposals'
    }
  },
  {
    path: 'advanced',
    canActivate: [
      AuthGuard
    ],
    component: ThesesAdvancedHomeComponent,
    children: [
        {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/current'
        },
        {
            path: 'list/:list',
            component: ThesesAdvancedTableComponent,
            resolve: {
                tableConfiguration: ThesesAdvancedTableConfigurationResolver,
                searchConfiguration: ThesesAdvancedTableSearchResolver
            },
            children: [
              {
                path: ':id/student/:studentId',
                component: ModalDetailsComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'StudentThesis',
                  action: 'edit',
                  closeOnSubmit: true,
                  continueLink: '.',
                }
              }
            ]
        }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThesesRoutingModule { }
